/// <reference types="react" />
export declare const StyledBlock: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const StyledContainer: import("styled-components").StyledComponent<({ className, children }: {
    className?: string;
    children: import("react").ReactNode;
}) => JSX.Element, any, {}, never>;
export declare const StyledContent: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const StyledLogo: import("styled-components").StyledComponent<() => JSX.Element, any, {}, never>;
export declare const StyledSearch: import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
