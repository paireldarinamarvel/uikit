/// <reference types="react" />
declare const Image: ({ src, srcset, alt, className }: {
    src: string;
    srcset: string;
    alt?: string;
    className?: string;
}) => JSX.Element;
export default Image;
