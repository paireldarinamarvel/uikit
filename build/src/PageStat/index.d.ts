/// <reference types="react" />
declare const PageStat: (props: {
    summ: any;
    className?: any;
    children: any;
}) => JSX.Element;
export default PageStat;
