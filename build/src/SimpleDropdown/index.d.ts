/// <reference types="react" />
declare const SimpleDropdown: ({ onChange, options, className }: {
    onChange: (value: any) => void;
    options: Array<any>;
    className?: any;
}) => JSX.Element;
export default SimpleDropdown;
