/// <reference types="react" />
declare const Order: ({ title, author, id, summ, nums, price, helper, status, reserves, date, quantity, quantityError, conditionId, dropExtraInfo, className }: {
    title: any;
    author: any;
    id?: any;
    summ: any;
    nums: any;
    price: any;
    helper: any;
    status: any;
    reserves: any;
    date: any;
    quantity: any;
    quantityError: any;
    conditionId: any;
    dropExtraInfo: any;
    className?: any;
}) => JSX.Element;
export default Order;
