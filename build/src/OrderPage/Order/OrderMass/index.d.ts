/// <reference types="react" />
declare const OrderMass: (({ className }: {
    className?: any;
}) => JSX.Element) & {
    displayName: string;
};
export default OrderMass;
