/// <reference types="react" />
export declare const Block: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Mass: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Selection: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Select: import("styled-components").StyledComponent<"label", any, {}, never>;
export declare const StyledCheckbox: import("styled-components").StyledComponent<({ num, checked, onChange, onReset, className, mod, variant }: {
    num?: number;
    checked?: boolean;
    onChange: any;
    onReset: any;
    className?: any;
    mod?: any;
    variant?: string;
}) => JSX.Element, any, {}, never>;
export declare const Nums: import("styled-components").StyledComponent<"span", any, {
    ishidden?: boolean;
}, never>;
export declare const Drops: import("styled-components").StyledComponent<"div", any, {
    ischecked?: boolean;
}, never>;
export declare const Drop: import("styled-components").StyledComponent<({ options, mod, placeholder, className, position, onChange }: {
    options?: any;
    mod?: any;
    placeholder: any;
    className?: any;
    position: any;
    onChange: any;
}) => JSX.Element, any, {}, never>;
export declare const AllActions: import("styled-components").StyledComponent<"div", any, {
    ischecked?: boolean;
}, never>;
export declare const Actions: import("styled-components").StyledComponent<"ul", any, {
    isvisible?: boolean;
    isopen?: boolean;
}, never>;
export declare const ActionsItem: import("styled-components").StyledComponent<"li", any, {}, never>;
export declare const Action: import("styled-components").StyledComponent<({ href, size, onClick, download, className, ariaLabel, disabled, type, mod, children }: {
    href?: string;
    size?: string;
    onClick?: any;
    download?: string;
    className?: string;
    ariaLabel?: string;
    disabled?: boolean;
    type?: string;
    mod?: "base" | "accent" | "accentLight" | "important" | "lighten" | "transparent";
    children: any;
}) => JSX.Element, any, {}, never>;
export declare const stylesIcn: import("styled-components").FlattenSimpleInterpolation;
export declare const ShipIcn: import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
export declare const ReloadIcn: import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
export declare const DuplicateIcn: import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
export declare const ShuffleIcn: import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
export declare const DelIcn: import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
export declare const ActionText: import("styled-components").StyledComponent<"span", any, {}, never>;
export declare const Overlay: import("styled-components").StyledComponent<"div", any, {
    isopened?: boolean;
}, never>;
