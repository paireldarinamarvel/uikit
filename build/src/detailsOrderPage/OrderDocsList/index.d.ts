/// <reference types="react" />
declare const OrderDocsList: ({ hasInvoice }: {
    hasInvoice: boolean;
}) => JSX.Element;
export default OrderDocsList;
