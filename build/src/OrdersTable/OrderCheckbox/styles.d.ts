import Checkbox from "../../Checkbox";
export declare const Block: import("styled-components").StyledComponent<"label", any, {}, never>;
export declare const StyledCheckbox: import("styled-components").StyledComponent<typeof Checkbox, any, {}, never>;
export declare const Nums: import("styled-components").StyledComponent<"span", any, {
    isselected?: any;
}, never>;
