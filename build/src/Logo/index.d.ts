/// <reference types="react" />
declare const Logo: ({ type, colorType }: {
    type?: "marvel" | "V-loader" | "loading";
    colorType?: "white-logo" | "blue-logo";
}) => JSX.Element;
export default Logo;
