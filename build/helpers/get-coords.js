function t(t){var e=t.getBoundingClientRect(),o=document.body,n=document.documentElement,l=window.pageYOffset||n.scrollTop||o.scrollTop,c=window.pageXOffset||n.scrollLeft||o.scrollLeft,f=n.clientTop||o.clientTop||0,d=n.clientLeft||o.clientLeft||0,r=e.top+l-f,i=e.left+c-d;return{top:Math.round(r),left:Math.round(i)}}export{t as default};
//# sourceMappingURL=get-coords.js.map
