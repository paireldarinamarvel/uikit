import e from"react";import{Block as t,StyledCheckbox as r,Nums as a}from"./styles.js";var c=function(c){var n=c.checked,s=c.setChecked,l=c.className;return e.createElement(t,{className:l},e.createElement(r,{nolabel:!0,onChange:function(){return s(!n)}}),e.createElement(a,{isselected:n},n?"1 резерв выбран":"Выбрать все"))};export{c as default};
//# sourceMappingURL=index.js.map
