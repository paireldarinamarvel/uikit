import e from"react";import{Block as t,Stats as a,Docs as m,Desc as r}from"./styles.js";var s=function(s){var c=s.className,n=s.item;return e.createElement(t,{className:c},e.createElement(a,{summ:n.summ}),e.createElement(m,{hasInvoice:n.hasInvoice}),e.createElement(r,{data:n}))};export{s as default};
//# sourceMappingURL=index.js.map
