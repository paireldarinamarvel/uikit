import e from"react";import{StyledSumm as t,StyledSummLabel as l,StyledSummPrice as r}from"./styles.js";var a=function(a){var m=a.summ,n=a.className,c=a.children;return e.createElement("div",{className:n},c,m&&e.createElement(t,null,e.createElement(l,null,"Итого:"),e.createElement(r,null,m)))};export{a as default};
//# sourceMappingURL=index.js.map
